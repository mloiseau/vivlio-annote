#!/usr/bin/python3
# -*- coding: UTF-8 -*-
# created by lzbk
import argparse
import os, sys, os.path
import getpass
import sqlite3
import json
#import bisect
import re #regular expression

parser = argparse.ArgumentParser(description="Extraire les notes d'une liseuse Vivlio")
parser.add_argument("-s", "--source",  help="le dossier de la liseuse", type=str, default=None)
parser.add_argument("-f","--format", help="le format d'export (JSON ou wiki)", type=str, default="json")
parser.add_argument("-d", "--destination", default=".", help="le chemin et nom du dossier d'export (un fichier par livre)", type=str)
args = parser.parse_args()


def getPage(anchor):
	output = re.search("page\=(\d+)",anchor)
	if output is not None:
		output = int(output.group(1))+1
	return output

def startChar(anchor):
	output = re.search("offs\=(\d+)",anchor)
	if output is not None:
		output = int(output.group(1))
	return output

def endChar(anchor):
	output = re.search("over\=(\d+)",anchor)
	if output is not None:
		output = int(output.group(1))
	return output

class Note:
	BM = 1 #bookmark
	QUOTE = 2 #citation
	NOTE = 3 #note

	def __init__(self, id, dbString):
		tmp = json.loads(dbString)
		self.text = None
		self.endP = None
		self.note = None
		self.startChar = None
		self.endChar = None
		self.id = id
		if len(tmp)==3:
			self.type = Note.NOTE
			tmp[0].update(tmp[1])
			tmp[0]["note"] = tmp[2]["text"].replace("\n","")
			self.note = tmp[0]["note"]
		elif len(tmp[1]) == 1:
			self.type = Note.BM
		else:
			tmp[0].update(tmp[1])
			self.type = Note.QUOTE
			#TODO mistakes Crayon for a quote, quoting "crayon"
		self.raw = tmp[0]
		self.p = getPage(tmp[0]["anchor"])
		if "begin" in tmp[0] :
			self.p = getPage(tmp[0]["begin"])
			self.startChar = startChar(tmp[0]["begin"])
		if "end" in tmp[0] :
			self.endP = getPage(tmp[0]["end"])
			self.endChar = endChar(tmp[0]["end"])
		if "text" in tmp[0] :
			self.text = tmp[0]["text"].replace("\n","")

	#for comparison
	def endP_int(self):
		res = self.endP
		if res == None:
			res=-1
		return res
	def startChar_int(self):
		res = self.startChar
		if res == None:
			res=-1
		return res
	def endChar_int(self):
		res = self.endChar
		if res == None:
			res=-1
		return res

	def __lt__(self, note2):
		return (self.p, self.startChar_int(), self.endP_int(), self.endChar_int(), self.type, self. id) < (note2.p, note2.startChar_int(), note2.endP_int(), note2.endChar_int(), note2.type, note2. id)

	def getType(self):
		if self.type == Note.BM:
			res = "Bookmark"
		elif self.type == Note.QUOTE:
			res = "Quote"
		else:
			res = "Note"
		return res

	def toJSON(self):
		json_note = {"id":self.id, "type":self.getType(),"p":self.p}
		if self.startChar!=None:
			json_note["startChar"] = self.startChar
		if self.endP != None :
			json_note["endP"] = self.endP
		if self.endChar!=None:
			json_note["endChar"] = self.endChar
		if self.text != None :
			json_note["text"] = self.text
		json_note["raw"] = self.raw
		return json_note

	def toWiki(self, writePageNumber = True):
		wiki_note = "<p id='"+self.getType()+"_"+str(self.id)+"'>"
		if self.type != Note.BM:
			if writePageNumber:
				wiki_note += "{{p|"+str(self.p)
				if self.endP != None and self.p!=self.endP:
					wiki_note += "–"+str(self.endP)
				wiki_note+="}}\n"
			if self.text != None:
				wiki_note += "«&nbsp;"+self.text+"&nbsp;»"
			if self.note != None:
				wiki_note += " {{commentaire|"+self.note+"}}"
		return wiki_note+"</p>"

class Book:
	def __init__(self, author=None, title=None):
		self.author = author
		self.title = title
		self.data = []

	def equals(self, author, title):
		return self.author == author and self.title == title

	def is_empty(self):
		return len(self.data)==0

	def empty(self):
		self.data = []

	def addNote(self, note):
		#bisect.insort_left(self.data,note)
		self.data.append(note)

	def fileName(self, directory="."):
		res = False
		if self.author == None:
			if self.title == None:
				raise ValueError("Can't export title and author-less book")
			else:
				res = self.title
		elif self.title == None:
			res = self.author
		else:
			res = self.author+"_"+self.title
		return directory+"/"+res

	def store(self, directory=".", format="json"):
		self.data.sort()
		if format == "json":
			self.JSON_store(self.fileName(directory)+".json")
		elif format == "wiki":
			self.wiki_store(self.fileName(directory)+".wiki")
		else:
			raise ValueError("Unexpected format: "+str(format))

	def JSON_store(self, fileName):
		content = {"author":self.author, "title":self.title, "data":[]}
		for note in self.data:
			content["data"].append(note.toJSON())
		out_file = open(fileName, "w")
		json.dump(content, out_file, indent = 2, ensure_ascii=False)
		out_file.close()

	def wiki_store(self, fileName):
		out_file = open(fileName, "w")
		out_file.write("="+self.author+" — "+self.title+"=\n")
		last_note = None
		for note in self.data:
			writePageNumber = not isinstance(last_note,Note) or (last_note.p != note.p or last_note.endP != note.endP)
			out_file.write(note.toWiki(writePageNumber))
			out_file.write("\n")
			last_note = note
		out_file.close()


#####
# Main functions
#####
def getSource():
	print("Warning, this only works for linux systems")
	basepath = "/media/"+getpass.getuser()
	files = os.listdir(basepath)
	i=1
	if len(files)>0:
		for f in files:
			print(str(i)+". "+f)
			i+=1
		drive = int(input("Select drive #: "))
		while (drive<1 or drive >= i):
			drive = int(input("Select drive #: "))
		res = basepath+"/"+files[drive - 1]
	else:
		print("No mounted drive")
		res=None
	return res

def connect(sourceDir):
	try:
		path = sourceDir+'/system/config/books.db'
		conn = sqlite3.connect("file://"+path+'?mode=ro', uri=True)
		print("Connected to '"+path+"'.")
	except (sqlite3.Error) as e:
		print (str(e))
		conn = None
	return conn

def retrieveData(conn):
	query = "SELECT `Books`.`Title` as `title`, `Books`.`Authors` as `author`, '['||GROUP_CONCAT(`Tags`.`Val`, ',')||']' as `data`, `Tags`.`ItemID` as `id` FROM `Books`, `Items`, `Tags` WHERE (`Tags`.`TagID` = 101 OR `Tags`.`TagID` = 104 OR `Tags`.`TagID` = 105) AND `Tags`.`ItemID`= `Items`.`OID` AND `Items`.`ParentID`=`Books`.`OID` GROUP BY `Items`.`OID` ORDER BY `author`, `title`, `Tags`.`TimeEdt`;"
	cur = conn.cursor()
	cur.execute(query)
	return cur.fetchall()
	#while conn.fetchone()

def export_data(data, directory, format="json"):
	curbook = Book()
	for comment in data:
		if not curbook.equals(comment[1], comment[0]):
			if not curbook.is_empty():
				curbook.store(directory, format)
				curbook.empty()
			curbook = Book(comment[1],comment[0])
		curbook.addNote(Note(comment[3],comment[2]))
	if not curbook.is_empty():
		curbook.store(directory, format)

#main
if __name__ == "__main__":
	if args.source == None:
		source = getSource()
	else:
		source = args.source
	if source != None:
		conn = connect(source)
		if conn != None:
			data = retrieveData(conn)
			conn.close()
			if data != None:
				try:
					export_data(data, args.destination, args.format)
				except ValueError as e:
					print(e)
