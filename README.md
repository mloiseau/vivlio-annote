# vivlio annotations

A small python script to extract annotations from vivlio e-reader.

Tested under linux mint, probably works with mac (requires option ``-s``) and possibly under windows with the appropriate parameters

## Usage
```bash
./extract_notes.py --help
usage: extract_notes.py [-h] [-s SOURCE] [-f FORMAT] [-d DESTINATION]
```
```
Extraire les notes d'une liseuse Vivlio

optional arguments:
  -h, --help            show this help message and exit
  -s SOURCE, --source SOURCE
                        le dossier de la liseuse (optional for linux, mandatory for other OSes)
  -f FORMAT, --format FORMAT
                        le format d'export (JSON ou wiki)
  -d DESTINATION, --destination DESTINATION
                        le chemin et nom du dossier d'export (un fichier par
                        livre)
```

### Example with all parameters
```bash
./extract_notes.py -s ~/Desktop/mountpoint -f wiki -d ~/Documents/Work/MyNotes
```
